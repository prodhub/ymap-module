# Yandex map wrap module


## Install

```sh
$ npm install --save @adwatch/ymap
```

## Usage

```js
import Ymap from '@adwatch/ymap';		// for es6

var Ymap = require('@adwatch/ymap/build');	// for es5

let ymap = new Ymap(options);
```

## Get started

```js
// jQuery

$(function() {

	let myMap = new Ymap({
		center: [55.76, 37.64],
		container: '#myMap',
		controls: ['default'],
		behaviors: ['default'],
		native: false,
   
		onInit: function (map) {
			myMap.addObjects({{objects array}});
		},
   
		onBoundsChange: function (map) {
			
		},
   
		objectCollectionParser: (obj)=>{    
			return {
				geometry: {
					// required
					coordinates: obj.coordinates
				},
				properties: {
					objectType: obj.type
				}
			}
		}
	});

});

```


## API

#### Options

| Name                       |   Type   |  Description |
| :------------------------- | :--------- | :------ |
| `center                  ` | `array   ` | Map center on initialize |
| `container               ` | `string  ` | Map container selector (required). Default - `#map` |
| `controls                ` | `array   ` | Control elements on map [link to api]( https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/control.Manager-docpage/#add-param-control). Default - `['default']` |
| `behaviors               ` | `array   ` | Map behaviors [link to api](https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/map.behavior.Manager-docpage/#param-behaviors). Default - `['default']` |
| `duration                ` | `number  ` | duration on scale change |
| `zoom                    ` | `number  ` | Map scale on initialize |
| `minZoom                 ` | `number  ` | minimum map scale |
| `maxZoom                 ` | `number  ` | maximum map scale |
| `objectManagerOptions    ` | `object  ` | ObjectManager parameters [link to api](https://tech.yandex.ru/maps/doc/jsapi/2.1-dev/ref/reference/ObjectManager-docpage/#param-options) |
| `native                  ` | `boolean ` | use native api methods (if you have many objects (more than 1000), recommended set this `false` ). Default - `true` |
| `objectCollectionParser  ` | `function` | yandex map objectManager parse function |
| `onInit                  ` | `function` | map init callback |
| `onBoundsChange          ` | `function` | change view map callback |


Options.[controls](https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/map.behavior.Manager-docpage/#param-behaviors) 
```
[
	'default',
	'fullscreenControl', 
	'geolocationControl', 
	'routeEditor', 
	'rulerControl',
	'searchControl', 
	'trafficControl', 
	'typeSelector', 
	'zoomControl'
]
```

Options.[behaviors](https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/map.behavior.Manager-docpage/#param-behaviors)
```
[
	'default',
	'drag',
	'scrollZoom',
	'dblClickZoom',
	'multiTouch',
	'rightMouseButtonMagnifier',
	'leftMouseButtonMagnifier',
	'ruler',
	'routeEditor'
]
```



### Callbacks option arguments

onInit: map init callback.

onBoundsChange: change view map callback.

objectCollectionParser: yandex map objectManager parse function 
[link to api](https://tech.yandex.ru/maps/doc/jsapi/2.1-dev/ref/reference/ObjectManager-docpage/#add-param-objects).

```js

option.objectCollectionParser = (obj)=>{    
		return {
			geometry: {
				// required
				coordinates: obj.coordinates
			},
			properties: {
				objectType: obj.type
			}
		};
	}

```



## Methods	

#### .init(options)
Build map.

### .destroy()
Destroy map.

### .addObjects(array)
Add new objects on map

### .clearObjects()
Remove all objects from map

### .updateObjects(array)
Clear old and add new objects on map

### .shownObjects()
Return array visible objects on map.  

### .setCenter(coordinates, zoom)

coordinates - coordinates array (ex. `[10, 20]`) or bounds array (`[[10,20], [15, 25]]`) 
zoom - map zoom (optional). 

### .setFilter(function)

function - filter function.


```js
// example

// set filter
myMap.setFilter(function (object) {
    return object.properties.objectType != 'shop';
});


// clear filter
myMap.setFilter();

```


## License

MIT © 
